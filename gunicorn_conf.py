import multiprocessing

bind = '0.0.0.0:8000'
backlog = 2048
workers = multiprocessing.cpu_count()
worker_class = 'uvicorn.workers.UvicornWorker'
threads = 1
worker_connections = 1000
chdir = '/home/ubuntu/team-finder/thingsfinder/tf-server'
daemon = False
pidfile = './log/gunicorn.pid'
accesslog = './log/gunicorn_access.log'
errorlog = './log/gunicorn.log'
loglevel = 'debug'
capture_output = True
#proc_name = 'main:app'
