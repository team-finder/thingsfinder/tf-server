# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 16:38:48 2021

@author: FYO
"""

from datetime import datetime
from enum import Enum
from typing import Optional, List

from fastapi import FastAPI, Path, HTTPException, status #, Response
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from parse import parse

import tflib as tf

class UserSearchBy(str, Enum):
    NAME = 'name'
    ACCOUNT = 'account'
    CARD_ID = 'cardid'
    MOBILE = 'mobile'
    EMAIL = 'email'

class ItemSearchBy(str, Enum):
    UNIQUE_ID = 'uniqueid'

class ProductSearchBy(str, Enum):
    NAME = 'name'

class InventoryStatus(str, Enum):
    FOUND_GOOD = '在库|完好'
    FOUND_BAD = '在库|损坏'
    FOUND_NOLABEL = '在库|无标'
    FOUND_NOCONTENT = '在库|无物'
    NOTFOUND_GOOD = '在外|完好'
    NOTFOUND_NOLABEL = '在外|无标'
    NOTFOUND_NOCONTENT = '在外|无物'
    PLACEHOLDER = '空号'

app = FastAPI(title='TF', description='ThingsFinder v0.2.1', version='0.2.1')
list_alarms = []

@app.on_event('startup')
def startup():
    print(f'[{datetime.now()}] Connecting...')
    tf.load_profile('profile.json')
    tf.connect_db('db_tftest')

@app.on_event('shutdown')
def shutdown():
    print(f'[{datetime.now()}] Disconnecting...')
    tf.db_conn.disconnect()
    tf.dump_profile('profile.json')

@app.get('/v1', summary='API v1')
def api_v1():
    return {'msg': 'TF API v1 service running'}

@app.post('/v1/tickets', status_code=status.HTTP_201_CREATED, summary='Generate unique one-time ticket', description='Access condition: operator')
def generate_ticket(token: str):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    ticket = tf.generate_ticket()
    tf.db_conn.commit()
    return {'ticket': ticket}

@app.get('/v1/users/brief/{user_id}', response_model=List[tf.UserPropertyBrief], summary='Get brief info of user(s)', description='Access condition: user')
def get_user_brief(*, user_id: int = Path(..., ge=0), token: str, searchby: Optional[UserSearchBy] = '', keyword: Optional[str] = ''):
    # Client authentication
    who = tf.authenticate_user(token, 'user')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    if user_id > 0:   # search by user_id
        users = tf.find_user(user_id=user_id)
    else:   # search by keyword
        if searchby == UserSearchBy.NAME:
            users = tf.find_user(name=keyword)
        elif searchby == UserSearchBy.ACCOUNT:
            users = tf.find_user(account=keyword)
        elif searchby == UserSearchBy.CARD_ID:
            users = tf.find_user(card_id=keyword)
        elif searchby == UserSearchBy.MOBILE:
            users = tf.find_user(mobile=keyword)
        elif searchby == UserSearchBy.EMAIL:
            users = tf.find_user(email=keyword)
        else:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Search pattern not found')
    
    if not users:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='User not found')
    
    userproperties = []
    for user in users:
        userproperties.append(user.property)
    tf.db_conn.commit()
    return userproperties

@app.get('/v1/users/{user_id}', response_model=List[tf.UserProperty], summary='Get detailed info of user(s)', description='Access condition: operator')
def get_user(*, user_id: int = Path(..., ge=0), token: str, searchby: Optional[UserSearchBy] = '', keyword: Optional[str] = ''):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    if user_id > 0:   # search by user_id
        users = tf.find_user(user_id=user_id)
    else:   # search by keyword
        if searchby == UserSearchBy.NAME:
            users = tf.find_user(name=keyword)
        elif searchby == UserSearchBy.ACCOUNT:
            users = tf.find_user(account=keyword)
        elif searchby == UserSearchBy.CARD_ID:
            users = tf.find_user(card_id=keyword)
        elif searchby == UserSearchBy.MOBILE:
            users = tf.find_user(mobile=keyword)
        elif searchby == UserSearchBy.EMAIL:
            users = tf.find_user(email=keyword)
        elif searchby == '':
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Search pattern not found')
    
    if not users:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='User not found')
    
    userproperties = []
    for user in users:
        userproperties.append(user.property)
    tf.db_conn.commit()
    return userproperties

@app.put('/v1/users/{user_id}', response_model=tf.UserProperty, summary='Update user. If not found, create one', description='Access condition: admin')
def update_user(*, user_id: int = Path(..., gt=0), token: str, user_property: tf.UserProperty):
    # Client authentication
    who = tf.authenticate_user(token, 'admin')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    user = tf.User(user_property)
    if user.property.user_id != user_id:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='User ID not identical')
    
    existed = tf.User.check_id(user_id)
    if existed:   # update
        result = user.to_db()
    else:   # create
        result = user.to_db(is_new=True)
    if not result:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail='DB error: ' + tf.db_conn.dberr)
    
    tf.add_user(user)
    tf.db_conn.commit()
    if existed:
        return user.property
    else:
        return JSONResponse(status_code=status.HTTP_201_CREATED, content=jsonable_encoder(user.property))

@app.delete('/v1/users/{user_id}', summary='Delete user', description='Access condition: admin')
def delete_user(*, user_id: int = Path(..., gt=0), token: str):
    # Client authentication
    who = tf.authenticate_user(token, 'admin')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    result = tf.delete_user(user_id)
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='User not found')
    
    tf.db_conn.commit()
    return {'msg': 'User deleted'}

@app.post('/v1/users', status_code=status.HTTP_201_CREATED, response_model=tf.UserProperty, summary='Create user. Ticket required', description='Access condition: admin')
def create_user(token: str, ticket: str, user_property: tf.UserProperty):
    # Client authentication
    who = tf.authenticate_user(token, 'admin')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    # Ensuring idempotence
    if not tf.check_ticket(ticket):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    user = tf.User(user_property)
    if user.property.user_id != 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='User ID not set to 0')
    
    result = user.to_db(is_new=True)
    if not result:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail='DB error: ' + tf.db_conn.dberr)
    
    tf.add_user(user)
    tf.db_conn.commit()
    return user.property

@app.get('/v1/items/{item_id}', response_model=List[tf.ItemProperty], summary='Get detailed info of item(s)', description='Access condition: user')
def get_item(*, item_id: int = Path(..., ge=0), token: str, searchby: Optional[ItemSearchBy] = '', keyword: Optional[str] = ''):
    # Client authentication
    who = tf.authenticate_user(token, 'user')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    if item_id > 0:   # search by item_id
        items = tf.find_item(item_id=item_id)
    else:   # search by unique_id in keyword
        if searchby == ItemSearchBy.UNIQUE_ID:
            items = tf.find_item(unique_id=keyword)
        elif searchby == '':
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Search pattern not found')
    
    if not items:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Item not found')
    
    itemproperties = []
    for item in items:
        itemproperties.append(item.property)
    tf.db_conn.commit()
    return itemproperties

@app.put('/v1/items/{item_id}', response_model=tf.ItemProperty, summary='Update item. If not found, create one', description='Access condition: operator')
def update_item(*, item_id: int = Path(..., gt=0), token: str, item_property: tf.ItemProperty):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    item = tf.Item(item_property)
    if item.property.item_id != item_id:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Item ID not identical')
    
    existed = tf.Item.check_id(item_id)
    if existed:   # update
        result = item.to_db()
    else:   # create
        result = item.to_db(is_new=True)
    if not result:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail='DB error: ' + tf.db_conn.dberr)
  
    tf.add_item(item)
    tf.db_conn.commit()
    if existed:
        return item.property
    else:
        return JSONResponse(status_code=status.HTTP_201_CREATED, content=jsonable_encoder(item.property))

@app.delete('/v1/items/{item_id}', summary='Delete item', description='Access condition: operator')
def delete_item(*, item_id: int = Path(..., gt=0), token: str):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    result = tf.delete_item(item_id)
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Item not found')
    
    tf.db_conn.commit()
    return {'msg': 'Item deleted'}

@app.post('/v1/items', status_code=status.HTTP_201_CREATED, response_model=tf.ItemProperty, summary='Create item. Ticket required', description='Access condition: operator')
def create_item(token: str, ticket: str, item_property: tf.ItemProperty):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    # Ensuring idempotence
    if not tf.check_ticket(ticket):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    item = tf.Item(item_property)
    if item.property.item_id != 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Item ID not set to 0')
    
    result = item.to_db(is_new=True)
    if not result:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail='DB error: ' + tf.db_conn.dberr)
    
    tf.add_item(item)
    tf.db_conn.commit()
    return item.property

@app.get('/v1/products/brief/{product_id}', response_model=List[tf.ProductPropertyBrief], summary='Get brief info of product(s)', description='Access condition: user')
def get_product_brief(*, product_id: int = Path(..., ge=0), token: str, searchby: Optional[ProductSearchBy] = '', keyword: Optional[str] = ''):
    # Client authentication
    who = tf.authenticate_user(token, 'user')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    if product_id > 0:   # search by product_id
        products = tf.find_product(product_id=product_id)
    else:   # search by product_name in keyword
        if searchby == ProductSearchBy.NAME:
            products = tf.find_product(name=keyword)
        elif searchby == '':
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Search pattern not found')
    
    if not products:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Product not found')
    
    productproperties = []
    for product in products:
        productproperties.append(product.property)
    tf.db_conn.commit()
    return productproperties

@app.get('/v1/products/{product_id}', response_model=List[tf.ProductProperty], summary='Get detailed info of product(s)', description='Access condition: operator')
def get_product(*, product_id: int = Path(..., ge=0), token: str, searchby: Optional[ProductSearchBy] = '', keyword: Optional[str] = ''):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    if product_id > 0:   # search by product_id
        products = tf.find_product(product_id=product_id)
    else:   # search by product_name in keyword
        if searchby == ProductSearchBy.NAME:
            products = tf.find_product(name=keyword)
        elif searchby == '':
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Search pattern not found')
    
    if not products:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Product not found')
    
    productproperties = []
    for product in products:
        productproperties.append(product.property)
    tf.db_conn.commit()
    return productproperties

@app.put('/v1/products/{product_id}', response_model=tf.ProductProperty, summary='Update product. If not found, create one', description='Access condition: operator')
def update_product(*, product_id: int = Path(..., gt=0), token: str, product_property: tf.ProductProperty):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    product = tf.Product(product_property)
    if product.property.product_id != product_id:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Product ID not identical')
    
    existed = tf.Product.check_id(product_id)
    if existed:   # update
        result = product.to_db()
    else:   # create
        result = product.to_db(is_new=True)
    if not result:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail='DB error: ' + tf.db_conn.dberr)
    
    tf.add_product(product)
    tf.db_conn.commit()
    if existed:
        return product.property
    else:
        return JSONResponse(status_code=status.HTTP_201_CREATED, content=jsonable_encoder(product.property))

@app.delete('/v1/products/{product_id}', summary='Delete product', description='Access condition: operator')
def delete_product(*, product_id: int = Path(..., gt=0), token: str):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    result = tf.delete_product(product_id)
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Product not found')
    
    tf.db_conn.commit()
    return {'msg': 'Product deleted'}

@app.post('/v1/products', status_code=status.HTTP_201_CREATED, response_model=tf.ProductProperty, summary='Create product. Ticket required', description='Access condition: operator')
def create_product(token: str, ticket: str, product_property: tf.ProductProperty):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    # Ensuring idempotence
    if not tf.check_ticket(ticket):
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    product = tf.Product(product_property)
    if product.property.product_id != 0:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='Product ID not set to 0')
    
    result = product.to_db(is_new=True)
    if not result:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail='DB error: ' + tf.db_conn.dberr)
    
    tf.add_product(product)
    tf.db_conn.commit()
    return product.property

@app.get('/v1/inventory/alarms', summary='Show alarms', description='Access condition: operator')
def show_alarm(token: str):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    return {'alarms': f'{list_alarms}'}

@app.put('/v1/inventory/alarms/{uniqueid}', summary='Add alarm', description='Access condition: operator')
def add_alarm(uniqueid: str, token: str):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    if uniqueid not in list_alarms:
        list_alarms.append(uniqueid)
    return {'msg': 'Alarm added'}

@app.delete('/v1/inventory/alarms/{uniqueid}', summary='Delete alarm', description='Access condition: operator')
def delete_alarm(uniqueid: str, token: str):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    if uniqueid in list_alarms:
        list_alarms.pop(list_alarms.index(uniqueid))
    return {'msg': 'Alarm deleted'}

@app.put('/v1/inventory/{item_id}', response_model=tf.ItemProperty, summary='Make an inventory', description='Access condition: operator')
def make_inventory(*, item_id: int = Path(..., ge=0), token: str, itemstatus: InventoryStatus, productid: int, uniqueid: Optional[str] = '', uniqueid4: Optional[str] = ''):
    # Client authentication
    who = tf.authenticate_user(token, 'operator')
    if not who:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Action forbidden')
    
    # Items in alarm list must be found and processed before inventory. After processing, it should be removed from the list
    if uniqueid in list_alarms:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail='Alarm !!!')
    
    # Search by item_id or uniqueid
    if item_id > 0:   # item_id
        items = tf.find_item(item_id=item_id)
        if not items:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Item<{item_id}> not found')
    else:   # uniqueid
        items = tf.find_item(unique_id=uniqueid)
        if not items:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Item uniqueid not found')
        if len(items) > 1:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Item uniqueid not valid')
    item = items[0]
    
    # Check if it has been done
    info_len = len(tf.dict_profile['inventory_info'])
    if item.property.notes[0:info_len] == tf.dict_profile['inventory_info']:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'Item<{item.property.item_id}> inventory conflicted')
    
    if (item_id > 0) and (uniqueid != ''):
        # Check if uniqueid is match
        dict_unique_id = {1: '', 2: '', 3: '', 4: ''}
        dict_unique_id[1] = item.property.unique_id.group_1
        dict_unique_id[2] = item.property.unique_id.group_2
        dict_unique_id[3] = item.property.unique_id.group_3
        dict_unique_id[4] = item.property.unique_id.group_4
        
        dict_parse_result = parse('id{group:1d}.{uniqueid}', uniqueid)
        if dict_unique_id[dict_parse_result['group']] != dict_parse_result['uniqueid']:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'Item<{item.property.item_id}> uniqueid not match')
    
    if uniqueid4 != '':
        # Check if uniqueid4 is match
        unique_id4 = item.property.unique_id.group_4
        if unique_id4 != '':   # ignore if previously not existed
            if ('id4.' + unique_id4) != uniqueid4:
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'Item<{item.property.item_id}> uniqueid4 not match')
        
        # Check if uniqueid4 is duplicated
        items2 = tf.find_item(unique_id=uniqueid4)
        if items2:
            if items2[0].property.item_id != item.property.item_id:
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'Item<{item.property.item_id}> uniqueid4 duplicated in item<{items2[0].property.item_id}>')
    
    # Check if productid is match
    if item.property.product_id != productid:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'Item<{item.property.item_id}> productid not match')
    
    # Get product category
    products = tf.find_product(product_id=productid)
    if not products:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'Item<{item.property.item_id}> productid not match')
    controlled_by = products[0].property.controlled_by
    
    # Set default value
    if uniqueid4 != '':
        item.property.unique_id.group_4 = uniqueid4[4:]
    item.property.checked_stuff = True
    if controlled_by == 1:
        item.property.checked_label = True
        item.property.checked_stock = True
    elif controlled_by == 2:
        item.property.checked_internal_label = True
        item.property.checked_internal_stock = True
    item.property.discarded = False
    
    # Set value
    if itemstatus == InventoryStatus.FOUND_GOOD:
        item.property.notes = tf.dict_profile['inventory_info']+'在库|完好'
        item.property.status = 'Good condition'
    elif itemstatus == InventoryStatus.FOUND_BAD:
        item.property.notes = tf.dict_profile['inventory_info']+'在库|损坏'
        item.property.status = 'Bad condition'
    elif itemstatus == InventoryStatus.FOUND_NOLABEL:
        item.property.notes = tf.dict_profile['inventory_info']+'在库|无标'
        item.property.status = 'No label'
        if controlled_by == 1:
            item.property.checked_label = False
            item.property.checked_stock = False
        elif controlled_by == 2:
            item.property.checked_internal_label = False
            item.property.checked_internal_stock = False
    elif itemstatus == InventoryStatus.FOUND_NOCONTENT:
        item.property.notes = tf.dict_profile['inventory_info']+'在库|无物'
        item.property.status = 'No content'
        item.property.checked_stuff = False
    elif itemstatus == InventoryStatus.NOTFOUND_GOOD:
        item.property.notes = tf.dict_profile['inventory_info']+'在外|完好'
        item.property.status = 'Good condition'
    elif itemstatus == InventoryStatus.NOTFOUND_NOLABEL:
        item.property.notes = tf.dict_profile['inventory_info']+'在外|无标'
        item.property.status = 'No label'
        if controlled_by == 1:
            item.property.checked_label = False
            item.property.checked_stock = False
        elif controlled_by == 2:
            item.property.checked_internal_label = False
            item.property.checked_internal_stock = False
    elif itemstatus == InventoryStatus.PLACEHOLDER:
        item.property.notes = tf.dict_profile['inventory_info']+'空号'
        item.property.status = 'Placeholder'
        item.property.checked_stuff = False
        if controlled_by == 1:
            item.property.checked_label = False
        elif controlled_by == 2:
            item.property.checked_internal_label = False
    else:   # InventoryStatus.NOTFOUND_NOCONTENT
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f'Item<{item.property.item_id}> status not processed')
    
    result = item.to_db()
    if not result:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail='DB error: ' + tf.db_conn.dberr)
  
    tf.add_item(item)
    tf.db_conn.commit()
    return item.property

