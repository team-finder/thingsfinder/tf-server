CREATE TABLE tf_staff (
  st_id int NOT NULL AUTO_INCREMENT,
  st_name varchar(255) NOT NULL,
  st_alias varchar(255) NOT NULL,
  st_account1 varchar(255) NOT NULL,
  st_account2 varchar(255) NOT NULL,
  st_role varchar(255) NOT NULL,
  st_cardtype varchar(255) NOT NULL,
  st_cardid varchar(255) NOT NULL,
  st_mobile varchar(255) NOT NULL,
  st_email varchar(255) NOT NULL,
  st_notes varchar(255) NOT NULL,
  st_privilege int NOT NULL,
  st_deactivated smallint NOT NULL,
  PRIMARY KEY (st_id)
);

CREATE TABLE tf_loanrecord (
  lr_id int NOT NULL AUTO_INCREMENT,
  lr_applicationno varchar(255) NOT NULL,
  lr_applicationtype varchar(255) NOT NULL,
  lr_date datetime NOT NULL,
  lr_operatorid int NOT NULL,
  lr_cc1id int NOT NULL,
  lr_cc2id int NOT NULL,
  lr_applicantid int NOT NULL,
  lr_applicantname varchar(255) NOT NULL,
  lr_notes varchar(255) NOT NULL,
  lr_onloan int NOT NULL,
  lr_validity smallint NOT NULL,
  PRIMARY KEY (lr_id),
  FOREIGN KEY (lr_operatorid) REFERENCES tf_staff (st_id),
  FOREIGN KEY (lr_cc1id) REFERENCES tf_staff (st_id),
  FOREIGN KEY (lr_cc2id) REFERENCES tf_staff (st_id),
  FOREIGN KEY (lr_applicantid) REFERENCES tf_staff (st_id)
);

CREATE TABLE tf_productinstorage (
  ps_id int NOT NULL AUTO_INCREMENT,
  ps_stuff varchar(255) NOT NULL,
  ps_controlledby smallint NOT NULL,
  ps_officialname varchar(255) NOT NULL,
  ps_officialmodel varchar(255) NOT NULL,
  ps_unit varchar(255) NOT NULL,
  ps_totalnumber int NOT NULL,
  ps_usednumber int NOT NULL,
  ps_storedin varchar(255) NOT NULL,
  ps_currencyunit varchar(3) NOT NULL,
  ps_cost decimal(28,8) NOT NULL,
  ps_notes varchar(255) NOT NULL,
  PRIMARY KEY (ps_id)
);

CREATE TABLE tf_pconstatistics (
  pc_productid int NOT NULL,
  pc_total int NOT NULL,
  pc_controlled int NOT NULL,
  pc_existed int NOT NULL,
  pc_onloan int NOT NULL,
  pc_instock int NOT NULL,
  pc_goodcondition int NOT NULL,
  pc_badcondition int NOT NULL,
  pc_nolabel int NOT NULL,
  pc_nocontent int NOT NULL,
  PRIMARY KEY (pc_productid),
  FOREIGN KEY (pc_productid) REFERENCES tf_productinstorage (ps_id)
);

CREATE TABLE tf_puncspending (
  pu_id int NOT NULL AUTO_INCREMENT,
  pu_date datetime NOT NULL,
  pu_operatorid int NOT NULL,
  pu_productid int NOT NULL,
  pu_number int NOT NULL,
  pu_location varchar(255) NOT NULL,
  pu_notes varchar(255) NOT NULL,
  PRIMARY KEY (pu_id),
  FOREIGN KEY (pu_operatorid) REFERENCES tf_staff (st_id),
  FOREIGN KEY (pu_productid) REFERENCES tf_productinstorage (ps_id)
);

CREATE TABLE tf_batchimport (
  ba_id int NOT NULL AUTO_INCREMENT,
  ba_date datetime NOT NULL,
  ba_from varchar(255) NOT NULL,
  ba_operatorid int NOT NULL,
  ba_productid int NOT NULL,
  ba_number int NOT NULL,
  ba_thumbnail varchar(255) NOT NULL,
  ba_originalidtype varchar(255) NOT NULL,
  ba_originalid varchar(255) NOT NULL,
  ba_currencyunit varchar(3) NOT NULL,
  ba_cost decimal(28,8) NOT NULL,
  PRIMARY KEY (ba_id),
  FOREIGN KEY (ba_operatorid) REFERENCES tf_staff (st_id),
  FOREIGN KEY (ba_productid) REFERENCES tf_productinstorage (ps_id)
);

CREATE TABLE tf_itemundercontrol (
  ic_id int NOT NULL AUTO_INCREMENT,
  ic_date datetime NOT NULL,
  ic_operatorid int NOT NULL,
  ic_responderid int NOT NULL,
  ic_productid int NOT NULL,
  ic_belongto int NOT NULL,
  ic_number int NOT NULL,
  ic_location varchar(255) NOT NULL,
  ic_notes varchar(255) NOT NULL,
  ic_status varchar(255) NOT NULL,
  ic_checked smallint NOT NULL,
  ic_discarded smallint NOT NULL,
  PRIMARY KEY (ic_id),
  FOREIGN KEY (ic_operatorid) REFERENCES tf_staff (st_id),
  FOREIGN KEY (ic_responderid) REFERENCES tf_staff (st_id),
  FOREIGN KEY (ic_productid) REFERENCES tf_productinstorage (ps_id)
);

CREATE TABLE tf_itemuniqueid (
  iu_itemid int NOT NULL,
  iu_group smallint NOT NULL,
  iu_uniqueid varchar(255) NOT NULL,
  PRIMARY KEY (iu_itemid, iu_group),
  FOREIGN KEY (iu_itemid) REFERENCES tf_itemundercontrol (ic_id)
);

CREATE TABLE tf_itemadditionalinfo (
  ia_itemid int NOT NULL,
  ia_group smallint NOT NULL,
  ia_additionalinfo varchar(255) NOT NULL,
  PRIMARY KEY (ia_itemid, ia_group),
  FOREIGN KEY (ia_itemid) REFERENCES tf_itemundercontrol (ic_id)
);

CREATE TABLE tf_itemonloan (
  il_id int NOT NULL AUTO_INCREMENT,
  il_checkoutid int NOT NULL,
  il_itemid int NOT NULL,
  il_productid int NOT NULL,
  il_number int NOT NULL,
  il_checkoutnotes varchar(255) NOT NULL,
  il_checkoutthumbnail varchar(255) NOT NULL,
  il_estimatedreturn datetime NOT NULL,
  il_checkinid int NOT NULL,
  il_checkinnotes varchar(255) NOT NULL,
  il_checkinthumbnail varchar(255) NOT NULL,
  il_status varchar(255) NOT NULL,
  PRIMARY KEY (il_id),
  FOREIGN KEY (il_checkoutid) REFERENCES tf_loanrecord (lr_id),
  FOREIGN KEY (il_itemid) REFERENCES tf_itemundercontrol (ic_id),
  FOREIGN KEY (il_productid) REFERENCES tf_productinstorage (ps_id),
  FOREIGN KEY (il_checkinid) REFERENCES tf_loanrecord (lr_id)
);

CREATE OR REPLACE
algorithm = UNDEFINED VIEW tfv_productinstorage AS
SELECT
  tf_productinstorage.ps_id AS ps_id,
  tf_productinstorage.ps_stuff AS ps_stuff,
  CONCAT_WS('-', tf_productinstorage.ps_officialname, tf_productinstorage.ps_officialmodel) AS ps_official
FROM
  tf_productinstorage;
