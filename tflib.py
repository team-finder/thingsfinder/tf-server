# -*- coding: utf-8 -*-
"""
Created on Tue May 11 22:36:43 2021

@author: FYO
"""

from datetime import datetime, date, timedelta, timezone
from decimal import Decimal
from enum import Enum
import json
from typing import Optional, List, Any
import uuid

from fastapi import HTTPException, status
import pandas as pd
from parse import parse
from pydantic import BaseModel, Field
import pymysql as pydb

class DbConnection(object):
    def __init__(self):
        self.stmt = ''
        self.nrows = 0
        self.dberr = ''
        self.__is_connected = False
    
    def connect(self, host, port, database, user, password):
        if self.__is_connected:
            self.cursor.close()
            self.db.close()
            self.__is_connected = False
        
        self.db = pydb.connect(host=host, port=port, database=database, user=user, password=password)
        self.cursor = self.db.cursor()
        self.__is_connected = True
    
    def disconnect(self):
        if self.__is_connected:
            self.cursor.close()
            self.db.close()
            self.__is_connected = False
    
    def execute(self, query: str, params: Optional[Any] = None, commit: bool = False, rollback: bool = False, fetch: bool = False):
        if not self.__is_connected:
            return False
        
        self.stmt = f'{query}; {params}'
        try:
            if params:
                self.nrows = self.cursor.execute(query, params)
            else:
                self.nrows = self.cursor.execute(query)
            if commit:
                self.db.commit()
        except Exception as err:
            self.dberr = f'{err} in [{self.stmt}]'
            print('DB ERROR: ' + self.dberr)
            if rollback:
                self.db.rollback()
            return False
        
        if fetch:
            return self.cursor.fetchall()
        else:
            return True
    
    def commit(self):
        if not self.__is_connected:
            return
        
        self.db.commit()
    
    def rollback(self):
        if not self.__is_connected:
            return
        
        self.db.rollback()

class UserProperty(BaseModel):
    user_id: int = Field(title='Unique user ID', ge=0)
    user_name: str
    alias: str
    account1: str
    account2: str
    role: str
    card_type: str
    card_id: str
    mobile: str
    email: str
    notes: str
    privilege: int = Field(ge=0)
    deactivated: bool
    
    class Config:
        schema_extra = {
            'example': {
                'user_id': 1001,
                'user_name': '张君宝',
                'alias': '张三',
                'account1': 'ZhangJunBao',
                'account2': 'ZJB',
                'role': '计算机学院/本科生',
                'card_type': '校园卡',
                'card_id': '0123456789',
                'mobile': '10123456789',
                'email': 'zhangjunbao@163.com',
                'notes': 'CS1801',
                'privilege': 0,
                'deactivated': False
            }
        }
    
    def __eq__(self, other):
        return ((self.user_id == other.user_id) and
                (self.user_name == other.user_name) and
                (self.alias == other.alias) and
                (self.account1 == other.account1) and
                (self.account2 == other.account2) and
                (self.role == other.role) and
                (self.card_type == other.card_type) and
                (self.card_id == other.card_id) and
                (self.mobile == other.mobile) and
                (self.email == other.email) and
                (self.notes == other.notes) and
                (self.privilege == other.privilege) and
                (self.deactivated == other.deactivated))
    
    def __ne__(self, other):
        return not self.__eq__(other)

class UserPropertyBrief(BaseModel):
    user_id: int = Field(title='Unique user ID', ge=0)
    user_name: str
    alias: str
    account1: str
    account2: str
    role: str
    deactivated: bool
    
    class Config:
        schema_extra = {
            'example': {
                'user_id': 1001,
                'user_name': '张君宝',
                'alias': '张三',
                'account1': 'ZhangJunBao',
                'account2': 'ZJB',
                'role': '本科生',
                'deactivated': False
            }
        }

class User(object):
    def __init__(self, user_property: UserProperty):
        self.object_type = 'User'
        self.property = user_property
        self.token = ''
        
        self.__is_null = False    # True: initial value
        self.__is_synced = True   # True: all values synced with db storage
    
    def __eq__(self, other):
        return ((self.object_type == other.object_type) and
                (self.property == other.property))
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __repr__(self):
        return '<User(id={self.property.user_id!r})>'.format(self=self)
    
    def from_db(self, user_id: int = 0) -> bool:
        '''
        从数据库读取属性值。如无参数，则使用对象原有的User ID来查询数据库.

        Parameters
        ----------
        user_id : int, optional
            用给定的User ID来查询数据库. The default is 0.

        Returns
        -------
        bool
            True - if succeeded.
            False - if failed.

        '''
        if user_id != 0:   # 若给定user_id，使用给定值
            self.property.user_id = user_id
        
        if self.property.user_id == 0:
            return False
        
        # 根据对象的user_id读取其他属性值
        rows = db_conn.execute("SELECT st_name, st_alias, st_account1, st_account2, st_role, st_cardtype, st_cardid, st_mobile, st_email, st_notes, st_privilege, st_deactivated FROM tf_staff WHERE st_id = %s", params=(self.property.user_id), fetch=True)
        if not rows:
            return False
        for row in rows:
            self.property.user_name = row[0]
            self.property.alias = row[1]
            self.property.account1 = row[2]
            self.property.account2 = row[3]
            self.property.role = row[4]
            self.property.card_type = row[5]
            self.property.card_id = row[6]
            self.property.mobile = row[7]
            self.property.email = row[8]
            self.property.notes = row[9]
            self.property.privilege = row[10]
            self.property.deactivated = bool(row[11])
        
        self.__is_null = False
        self.__is_synced = True
        return True
    
    def to_db(self, is_new: bool = False) -> bool:
        '''
        向数据库中写入对象属性值。如为新记录，则创建新记录.

        Parameters
        ----------
        is_new : bool, optional
            是否为新记录. The default is False.

        Returns
        -------
        bool
            True - if succeeded.
            False - if failed.

        '''
        if is_new:   # insert
            result = db_conn.execute("INSERT INTO tf_staff (st_id, st_name, st_alias, st_account1, st_account2, st_role, st_cardtype, st_cardid, st_mobile, st_email, st_notes, st_privilege, st_deactivated) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", params=(self.property.user_id, self.property.user_name, self.property.alias, self.property.account1, self.property.account2, self.property.role, self.property.card_type, self.property.card_id, self.property.mobile, self.property.email, self.property.notes, self.property.privilege, int(self.property.deactivated)), commit=True, rollback=True)
            if not result:
                return False
            self.property.user_id = db_conn.cursor.lastrowid
        else:   # update
            result = db_conn.execute("UPDATE tf_staff SET st_name=%s, st_alias=%s, st_account1=%s, st_account2=%s, st_role=%s, st_cardtype=%s, st_cardid=%s, st_mobile=%s, st_email=%s, st_notes=%s, st_privilege=%s, st_deactivated=%s WHERE st_id=%s", params=(self.property.user_name, self.property.alias, self.property.account1, self.property.account2, self.property.role, self.property.card_type, self.property.card_id, self.property.mobile, self.property.email, self.property.notes, self.property.privilege, int(self.property.deactivated), self.property.user_id), commit=True, rollback=True)
            if not result:
                return False
        
        self.__is_null = False
        self.__is_synced = True
        return True
    
    @classmethod
    def check_id(cls, user_id: int) -> bool:
        '''
        Check if the given user ID existed.

        Parameters
        ----------
        user_id : int
            User ID.

        Returns
        -------
        bool
            True - if existed.
            False - if not existed.

        '''
        rows = db_conn.execute("SELECT Count(*) FROM tf_staff WHERE st_id = %s", params=(user_id), fetch=True)
        if rows:
            for row in rows:
                if row[0] == 1:
                    return True
        
        return False
    
    @classmethod
    def search_id(cls, name: str = '', account: str = '', card_id: str = '', mobile: str = '', email: str = '') -> List[int]:
        '''
        给定属性值，在数据库中查找对应User ID.

        Parameters
        ----------
        name : str, optional
            用给定的Name模糊查找对应User ID. The default is ''.
        account : str, optional
            用给定的Account模糊查找对应User ID. The default is ''.
        card_id : str, optional
            用给定的CardID模糊查找对应User ID. The default is ''.
        mobile : str, optional
            用给定的Mobile模糊查找对应User ID. The default is ''.
        email : str, optional
            用给定的Email模糊查找对应User ID. The default is ''.

        Returns
        -------
        List[int]
            List of user ID.

        '''
        userids = []
        
        if name != '':   # 通过name查找user_id
            rows = db_conn.execute("SELECT st_id FROM tf_staff WHERE (st_name LIKE %s) OR (st_alias LIKE %s)", params=('%'+name+'%', '%'+name+'%'), fetch=True)
            if rows:
                for row in rows:
                    userids.append(row[0])
        elif account != '':   # 通过account查找user_id
            rows = db_conn.execute("SELECT st_id FROM tf_staff WHERE (st_account1 LIKE %s) OR (st_account2 LIKE %s)", params=('%'+account+'%', '%'+account+'%'), fetch=True)
            if rows:
                for row in rows:
                    userids.append(row[0])
        elif card_id != '':   # 通过card_id查找user_id
            rows = db_conn.execute("SELECT st_id FROM tf_staff WHERE st_cardid LIKE %s", params=('%'+card_id+'%'), fetch=True)
            if rows:
                for row in rows:
                    userids.append(row[0])
        elif mobile != '':   # 通过mobile查找user_id
            rows = db_conn.execute("SELECT st_id FROM tf_staff WHERE st_mobile LIKE %s", params=('%'+mobile+'%'), fetch=True)
            if rows:
                for row in rows:
                    userids.append(row[0])
        elif email != '':   # 通过email查找user_id
            rows = db_conn.execute("SELECT st_id FROM tf_staff WHERE st_email LIKE %s", params=('%'+email+'%'), fetch=True)
            if rows:
                for row in rows:
                    userids.append(row[0])
        
        return userids

class ItemUniqueId(BaseModel):
    group_1: str
    group_2: str
    group_3: str
    group_4: str
    
    def __eq__(self, other):
        return ((self.group_1 == other.group_1) and
                (self.group_2 == other.group_2) and
                (self.group_3 == other.group_3) and
                (self.group_4 == other.group_4))
    
    def __ne__(self, other):
        return not self.__eq__(other)

class ItemAdditionalInfo(BaseModel):
    group_1: str
    group_2: str
    group_3: str
    group_4: str
    group_5: str
    
    def __eq__(self, other):
        return ((self.group_1 == other.group_1) and
                (self.group_2 == other.group_2) and
                (self.group_3 == other.group_3) and
                (self.group_4 == other.group_4) and
                (self.group_5 == other.group_5))
    
    def __ne__(self, other):
        return not self.__eq__(other)

class ItemStatus(str, Enum):
    GOOD_CONDITION = 'Good condition'
    BAD_CONDITION = 'Bad condition'
    NO_LABEL = 'No label'
    NO_CONTENT = 'No content'
    PLACEHOLDER = 'Placeholder'

class ItemProperty(BaseModel):
    item_id: int = Field(title='Unique item ID', ge=0)
    product_id: int = Field(gt=0)
    product_name: str
    unique_id: ItemUniqueId
    date: date
    operator_id: int = Field(gt=0)
    operator_name: str
    responder_id: int = Field(gt=0)
    responder_name: str
    belong_to: int = Field(ge=0)
    number: int = Field(ge=0)
    location: str
    notes: str
    status: ItemStatus
    checked_stuff: bool
    checked_label: bool
    checked_stock: bool
    checked_internal_label: bool
    checked_internal_stock: bool
    discarded: bool
    additional_info: ItemAdditionalInfo
    
    class Config:
        schema_extra = {
            'example': {
                'item_id': 10001,
                'product_id': 101,
                'product_name': 'DDR开发板',
                'unique_id': {
                    'group_1': '',
                    'group_2': '01234567',
                    'group_3': '',
                    'group_4': 'D012345'
                },
                'date': '2020-12-31',
                'operator_id': 1002,
                'operator_name': '李四',
                'responder_id': 1003,
                'responder_name': '王五',
                'belong_to': 0,
                'number': 1,
                'location': '301室',
                'notes': '集中采购',
                'status': 'Good condition',
                'checked_stuff': True,
                'checked_label': True,
                'checked_stock': False,
                'checked_internal_label': False,
                'checked_internal_stock': False,
                'discarded': False,
                'additional_info': {
                    'group_1': '实验中心',
                    'group_2': '301室',
                    'group_3': '在用',
                    'group_4': '2017-07-31',
                    'group_5': ''
                }
            }
        }
    
    def __eq__(self, other):
        return ((self.item_id == other.item_id) and
                (self.product_id == other.product_id) and
                (self.product_name == other.product_name) and
                (self.unique_id == other.unique_id) and
                (self.date == other.date) and
                (self.operator_id == other.operator_id) and
                (self.operator_name == other.operator_name) and
                (self.responder_id == other.responder_id) and
                (self.responder_name == other.responder_name) and
                (self.belong_to == other.belong_to) and
                (self.number == other.number) and
                (self.location == other.location) and
                (self.notes == other.notes) and
                (self.status == other.status) and
                (self.checked_stuff == other.checked_stuff) and
                (self.checked_label == other.checked_label) and
                (self.checked_stock == other.checked_stock) and
                (self.checked_internal_label == other.checked_internal_label) and
                (self.checked_internal_stock == other.checked_internal_stock) and
                (self.discarded == other.discarded) and
                (self.additional_info == other.additional_info))
    
    def __ne__(self, other):
        return not self.__eq__(other)

class Item(object):
    def __init__(self, item_property: ItemProperty):
        self.object_type = 'Item'
        self.property = item_property
        
        self.__is_null = True      # True: initial value
        self.__is_synced = False   # True: all values synced with db storage
    
    def __eq__(self, other):
        return ((self.object_type == other.object_type) and
                (self.property == other.property))
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __repr__(self):
        return '<Item(id={self.property.item_id!r})>'.format(self=self)
    
    def from_db(self, item_id: int = 0) -> bool:
        '''
        从数据库读取属性值。如无参数，则使用对象原有的Item ID来查询数据库.

        Parameters
        ----------
        item_id : int, optional
            用给定的Item ID来查询数据库. The default is 0.

        Returns
        -------
        bool
            True - if succeeded.
            False - if failed.

        '''
        if item_id != 0:   # 若给定item_id，使用给定值
            self.property.item_id = item_id
        
        if self.property.item_id == 0:
            return False
        
        # 根据对象的item_id读取其他属性值
        rows = db_conn.execute("SELECT ic_date, ic_operatorid, ic_responderid, ic_productid, ic_belongto, ic_number, ic_location, ic_notes, ic_status, ic_checked, ic_discarded, ps.ps_stuff, sa.st_name, sb.st_name FROM tf_itemundercontrol ic LEFT JOIN tf_productinstorage ps ON ic.ic_productid = ps.ps_id LEFT JOIN tf_staff sa ON ic.ic_operatorid = sa.st_id LEFT JOIN tf_staff sb ON ic.ic_responderid = sb.st_id WHERE ic_id = %s", params=(self.property.item_id), fetch=True)
        if not rows:
            return False
        for row in rows:
            self.property.date = row[0].date()
            self.property.operator_id = row[1]
            self.property.responder_id = row[2]
            self.property.product_id = row[3]
            self.property.belong_to = row[4]
            self.property.number = row[5]
            self.property.location = row[6]
            self.property.notes = row[7]
            self.property.status = row[8]
            checked = []
            checked.extend(bin(row[9])[2:].rjust(8, '0'))
            self.property.checked_stuff = bool(int(checked[7]))
            self.property.checked_label = bool(int(checked[6]))
            self.property.checked_stock = bool(int(checked[5]))
            self.property.checked_internal_label = bool(int(checked[4]))
            self.property.checked_internal_stock = bool(int(checked[3]))
            self.property.discarded = bool(row[10])
            self.property.product_name = row[11]
            self.property.operator_name = row[12]
            self.property.responder_name = row[13]
        
        dict_unique_id = {1: '', 2: '', 3: '', 4: ''}
        rows = db_conn.execute("SELECT iu_group, iu_uniqueid FROM tf_itemuniqueid WHERE iu_itemid = %s", params=(self.property.item_id), fetch=True)
        if rows:
            for row in rows:
                dict_unique_id[row[0]] = row[1]
        self.property.unique_id.group_1 = dict_unique_id[1]
        self.property.unique_id.group_2 = dict_unique_id[2]
        self.property.unique_id.group_3 = dict_unique_id[3]
        self.property.unique_id.group_4 = dict_unique_id[4]
        
        dict_additional_info = {1: '', 2: '', 3: '', 4: '', 5: ''}
        rows = db_conn.execute("SELECT ia_group, ia_additionalinfo FROM tf_itemadditionalinfo WHERE ia_itemid = %s", params=(self.property.item_id), fetch=True)
        if rows:
            for row in rows:
                dict_additional_info[row[0]] = row[1]
        self.property.additional_info.group_1 = dict_additional_info[1]
        self.property.additional_info.group_2 = dict_additional_info[2]
        self.property.additional_info.group_3 = dict_additional_info[3]
        self.property.additional_info.group_4 = dict_additional_info[4]
        self.property.additional_info.group_5 = dict_additional_info[5]
        
        self.__is_null = False
        self.__is_synced = True
        return True
    
    def to_db(self, is_new: bool = False) -> bool:
        '''
        向数据库中写入对象属性值。如为新记录，则创建新记录.

        Parameters
        ----------
        is_new : bool, optional
            是否为新记录. The default is False.

        Returns
        -------
        bool
            True - if succeeded.
            False - if failed.

        '''
        checked = 0
        if self.property.checked_stuff:
            checked += 1
        if self.property.checked_label:
            checked += 2
        if self.property.checked_stock:
            checked += 4
        if self.property.checked_internal_label:
            checked += 8
        if self.property.checked_internal_stock:
            checked += 16
        
        dict_unique_id = {}
        dict_unique_id[1] = self.property.unique_id.group_1
        dict_unique_id[2] = self.property.unique_id.group_2
        dict_unique_id[3] = self.property.unique_id.group_3
        dict_unique_id[4] = self.property.unique_id.group_4
        
        dict_additional_info = {}
        dict_additional_info[1] = self.property.additional_info.group_1
        dict_additional_info[2] = self.property.additional_info.group_2
        dict_additional_info[3] = self.property.additional_info.group_3
        dict_additional_info[4] = self.property.additional_info.group_4
        dict_additional_info[5] = self.property.additional_info.group_5
        
        if is_new:   # insert
            result = db_conn.execute("INSERT INTO tf_itemundercontrol (ic_id, ic_date, ic_operatorid, ic_responderid, ic_productid, ic_belongto, ic_number, ic_location, ic_notes, ic_status, ic_checked, ic_discarded) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", params=(self.property.item_id, self.property.date, self.property.operator_id, self.property.responder_id, self.property.product_id, self.property.belong_to, self.property.number, self.property.location, self.property.notes, self.property.status, checked, int(self.property.discarded)), rollback=True)
            if not result:
                return False
            self.property.item_id = db_conn.cursor.lastrowid
            
            for group in range(1, 5):
                if dict_unique_id[group] != '':
                    result = db_conn.execute("INSERT INTO tf_itemuniqueid (iu_itemid, iu_group, iu_uniqueid) VALUES(%s, %s, %s)", params=(self.property.item_id, group, dict_unique_id[group]), rollback=True)
                    if not result:
                        return False
            
            for group in range(1, 6):
                if dict_additional_info[group] != '':
                    result = db_conn.execute("INSERT INTO tf_itemadditionalinfo (ia_itemid, ia_group, ia_additionalinfo) VALUES(%s, %s, %s)", params=(self.property.item_id, group, dict_additional_info[group]), rollback=True)
                    if not result:
                        return False
        else:   # update
            result = db_conn.execute("UPDATE tf_itemundercontrol SET ic_date=%s, ic_operatorid=%s, ic_responderid=%s, ic_productid=%s, ic_belongto=%s, ic_number=%s, ic_location=%s, ic_notes=%s, ic_status=%s, ic_checked=%s, ic_discarded=%s WHERE ic_id=%s", params=(self.property.date, self.property.operator_id, self.property.responder_id, self.property.product_id, self.property.belong_to, self.property.number, self.property.location, self.property.notes, self.property.status, checked, int(self.property.discarded), self.property.item_id), rollback=True)
            if not result:
                return False
            
            result = db_conn.execute("DELETE FROM tf_itemuniqueid WHERE iu_itemid=%s", params=(self.property.item_id), rollback=True)
            if not result:
                return False
            for group in range(1, 5):
                if dict_unique_id[group] != '':
                    result = db_conn.execute("INSERT INTO tf_itemuniqueid (iu_itemid, iu_group, iu_uniqueid) VALUES(%s, %s, %s)", params=(self.property.item_id, group, dict_unique_id[group]), rollback=True)
                    if not result:
                        return False
            
            result = db_conn.execute("DELETE FROM tf_itemadditionalinfo WHERE ia_itemid=%s", params=(self.property.item_id), rollback=True)
            if not result:
                return False
            for group in range(1, 6):
                if dict_additional_info[group] != '':
                    result = db_conn.execute("INSERT INTO tf_itemadditionalinfo (ia_itemid, ia_group, ia_additionalinfo) VALUES(%s, %s, %s)", params=(self.property.item_id, group, dict_additional_info[group]), rollback=True)
                    if not result:
                        return False
        
        db_conn.commit()
        self.__is_null = False
        self.__is_synced = True
        return True
    
    @classmethod
    def check_id(cls, item_id: int) -> bool:
        '''
        Check if the given item ID existed.

        Parameters
        ----------
        item_id : int
            Item ID.

        Returns
        -------
        bool
            True - if existed.
            False - if not existed.

        '''
        rows = db_conn.execute("SELECT Count(*) FROM tf_itemundercontrol WHERE ic_id = %s", params=(item_id), fetch=True)
        if rows:
            for row in rows:
                if row[0] == 1:
                    return True
        
        return False
    
    @classmethod
    def search_id(cls, unique_id: str) -> List[int]:
        '''
        给定Unique ID，在数据库中查找对应Item ID.

        Parameters
        ----------
        unique_id : str
            用给定的Unique ID模糊查找对应Item ID.

        Returns
        -------
        List[int]
            List of item ID.

        '''
        itemids = []
        
        dict_parse_result = parse('id{group:1d}.{uniqueid}', unique_id)
        if not dict_parse_result:
            return itemids
        rows = db_conn.execute("SELECT iu_itemid FROM tf_itemuniqueid WHERE (iu_group = %s) AND (iu_uniqueid LIKE %s)", params=(dict_parse_result['group'], '%'+dict_parse_result['uniqueid']+'%'), fetch=True)
        if rows:
            for row in rows:
                itemids.append(row[0])
        
        return itemids

class ProductStatistics(BaseModel):
    total: int = Field(ge=0)
    controlled: int = Field(ge=0)
    existed: int = Field(ge=0)
    on_loan: int = Field(ge=0)
    in_stock: int = Field(ge=0)
    good_condition: int = Field(ge=0)
    bad_condition: int = Field(ge=0)
    no_label: int = Field(ge=0)
    no_content: int = Field(ge=0)
    
    def __eq__(self, other):
        return ((self.total == other.total) and
                (self.controlled == other.controlled) and
                (self.existed == other.existed) and
                (self.on_loan == other.on_loan) and
                (self.in_stock == other.in_stock) and
                (self.good_condition == other.good_condition) and
                (self.bad_condition == other.bad_condition) and
                (self.no_label == other.no_label) and
                (self.no_content == other.no_content))
    
    def __ne__(self, other):
        return not self.__eq__(other)

class ProductProperty(BaseModel):
    product_id: int = Field(title='Unique product ID', ge=0)
    product_name: str
    controlled_by: int = Field(ge=0)
    official_name: str
    official_model: str
    unit: str
    currency_unit: str
    cost: Decimal
    notes: str
    statistics: ProductStatistics
    
    class Config:
        schema_extra = {
            'example': {
                'product_id': 101,
                'product_name': 'DDR开发板',
                'controlled_by': 1,
                'official_name': '开发板',
                'official_model': 'DDR',
                'unit': '块',
                'currency_unit': 'CNY',
                'cost': 1234.56,
                'notes': '',
                'statistics': {
                    'total': 100,
                    'controlled': 100,
                    'existed': 97,
                    'on_loan': 17,
                    'in_stock': 80,
                    'good_condition': 78,
                    'bad_condition': 2,
                    'no_label': 0,
                    'no_content': 0
                }
            }
        }
    
    def __eq__(self, other):
        return ((self.product_id == other.product_id) and
                (self.product_name == other.product_name) and
                (self.controlled_by == other.controlled_by) and
                (self.official_name == other.official_name) and
                (self.official_model == other.official_model) and
                (self.unit == other.unit) and
                (self.currency_unit == other.currency_unit) and
                (self.cost == other.cost) and
                (self.notes == other.notes) and
                (self.statistics == other.statistics))
    
    def __ne__(self, other):
        return not self.__eq__(other)

class ProductPropertyBrief(BaseModel):
    product_id: int = Field(title='Unique product ID', ge=0)
    product_name: str
    controlled_by: int = Field(ge=0)
    official_name: str
    official_model: str
    unit: str
    
    class Config:
        schema_extra = {
            'example': {
                'product_id': 101,
                'product_name': 'DDR开发板',
                'controlled_by': 1,
                'official_name': '开发板',
                'official_model': 'DDR',
                'unit': '块'
            }
        }

class Product(object):
    def __init__(self, product_property: ProductProperty):
        self.object_type = 'Product'
        self.property = product_property
        
        self.__is_null = True      # True: initial value
        self.__is_synced = False   # True: all values synced with db storage
    
    def __eq__(self, other):
        return ((self.object_type == other.object_type) and
                (self.property == other.property))
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __repr__(self):
        return '<Product(id={self.property.product_id!r})>'.format(self=self)
    
    def from_db(self, product_id: int = 0) -> bool:
        '''
        从数据库读取属性值。如无参数，则使用对象原有的Product ID来查询数据库.

        Parameters
        ----------
        product_id : int, optional
            用给定的Product ID来查询数据库. The default is 0.

        Returns
        -------
        bool
            True - if succeeded.
            False - if failed.

        '''
        if product_id != 0:   # 若给定product_id，使用给定值
            self.property.product_id = product_id
        
        if self.property.product_id == 0:
            return False
        
        # 根据对象的product_id读取其他属性值
        rows = db_conn.execute("SELECT ps_stuff, ps_controlledby, ps_officialname, ps_officialmodel, ps_unit, ps_currencyunit, ps_cost, ps_notes FROM tf_productinstorage WHERE ps_id = %s", params=(self.property.product_id), fetch=True)
        if not rows:
            return False
        for row in rows:
            self.property.product_name = row[0]
            self.property.controlled_by = row[1]
            self.property.official_name = row[2]
            self.property.official_model = row[3]
            self.property.unit = row[4]
            self.property.currency_unit = row[5]
            self.property.cost = round(row[6], 2)
            self.property.notes = row[7]
        
        if self.property.controlled_by == 0:   # 不属于受控product
            return False
        
        rows = db_conn.execute("SELECT pc_total, pc_controlled, pc_existed, pc_onloan, pc_instock, pc_goodcondition, pc_badcondition, pc_nolabel, pc_nocontent FROM tf_pconstatistics WHERE pc_productid = %s", params=(self.property.product_id), fetch=True)
        if rows:
            for row in rows:
                self.property.statistics.total = row[0]
                self.property.statistics.controlled = row[1]
                self.property.statistics.existed = row[2]
                self.property.statistics.on_loan = row[3]
                self.property.statistics.in_stock = row[4]
                self.property.statistics.good_condition = row[5]
                self.property.statistics.bad_condition = row[6]
                self.property.statistics.no_label = row[7]
                self.property.statistics.no_content = row[8]
        
        self.__is_null = False
        self.__is_synced = True
        return True

    def to_db(self, is_new: bool = False) -> bool:
        '''
        向数据库中写入对象属性值。如为新记录，则创建新记录.

        Parameters
        ----------
        is_new : bool, optional
            是否为新记录. The default is False.

        Returns
        -------
        bool
            True - if succeeded.
            False - if failed.

        '''
        if is_new:   # insert
            result = db_conn.execute("INSERT INTO tf_productinstorage (ps_id, ps_stuff, ps_controlledby, ps_officialname, ps_officialmodel, ps_unit, ps_totalnumber, ps_usednumber, ps_storedin, ps_currencyunit, ps_cost, ps_notes) VALUES(%s, %s, %s, %s, %s, %s, 0, 0, '', %s, %s, %s)", params=(self.property.product_id, self.property.product_name, self.property.controlled_by, self.property.official_name, self.property.official_model, self.property.unit, self.property.currency_unit, self.property.cost, self.property.notes), rollback=True)
            if not result:
                return False
            self.property.product_id = db_conn.cursor.lastrowid
            
            result = db_conn.execute("INSERT INTO tf_pconstatistics (pc_productid, pc_total, pc_controlled, pc_existed, pc_onloan, pc_instock, pc_goodcondition, pc_badcondition, pc_nolabel, pc_nocontent) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", params=(self.property.product_id, self.property.statistics.total, self.property.statistics.controlled, self.property.statistics.existed, self.property.statistics.on_loan, self.property.statistics.in_stock, self.property.statistics.good_condition, self.property.statistics.bad_condition, self.property.statistics.no_label, self.property.statistics.no_content), rollback=True)
            if not result:
                return False
        else:   # update
            result = db_conn.execute("UPDATE tf_productinstorage SET ps_stuff=%s, ps_controlledby=%s, ps_officialname=%s, ps_officialmodel=%s, ps_unit=%s, ps_totalnumber=0, ps_usednumber=0, ps_storedin='', ps_currencyunit=%s, ps_cost=%s, ps_notes=%s WHERE ps_id=%s", params=(self.property.product_name, self.property.controlled_by, self.property.official_name, self.property.official_model, self.property.unit, self.property.currency_unit, self.property.cost, self.property.notes, self.property.product_id), rollback=True)
            if not result:
                return False
            
            result = db_conn.execute("UPDATE tf_pconstatistics SET pc_total=%s, pc_controlled=%s, pc_existed=%s, pc_onloan=%s, pc_instock=%s, pc_goodcondition=%s, pc_badcondition=%s, pc_nolabel=%s, pc_nocontent=%s WHERE pc_productid=%s", params=(self.property.statistics.total, self.property.statistics.controlled, self.property.statistics.existed, self.property.statistics.on_loan, self.property.statistics.in_stock, self.property.statistics.good_condition, self.property.statistics.bad_condition, self.property.statistics.no_label, self.property.statistics.no_content, self.property.product_id), rollback=True)
            if not result:
                return False
        
        db_conn.commit()
        self.__is_null = False
        self.__is_synced = True
        return True
    
    @classmethod
    def check_id(cls, product_id: int) -> bool:
        '''
        Check if the given product ID existed.

        Parameters
        ----------
        product_id : int
            Product ID.

        Returns
        -------
        bool
            True - if existed.
            False - if not existed.

        '''
        rows = db_conn.execute("SELECT Count(*) FROM tf_productinstorage WHERE ps_id = %s", params=(product_id), fetch=True)
        if rows:
            for row in rows:
                if row[0] == 1:
                    return True
        
        return False
    
    @classmethod
    def search_id(cls, name: str) -> List[int]:
        '''
        按name搜索关键字，获取对应product ID.

        Parameters
        ----------
        name : str
            模式搜索product name，或者official name + official model

        Returns
        -------
        List[int]
            List of product ID.

        '''
        productids = []
        
        rows = db_conn.execute("SELECT ps_id FROM tfv_productinstorage WHERE (ps_stuff like %s) or (ps_official like %s)", params=('%'+name+'%', '%'+name+'%'), fetch=True)
        if rows:
            for row in rows:
                productids.append(row[0])
        
        return productids

def convert_to_new_tz(cur_time: datetime, cur_tz: int, new_tz: int) -> datetime:
    '''
    将原有时区的时间转换为新时区的时间.

    Parameters
    ----------
    cur_time : datetime
        待转换的时间.
    cur_tz : int
        待转换的时区.
    new_tz : int
        新的时区.

    Returns
    -------
    datetime
        新的时间.

    '''
    # 设置初始时区
    cur_time = cur_time.replace(tzinfo=timezone(timedelta(hours=cur_tz)))
    
    # 切换为新时区
    new_time = cur_time.astimezone(timezone(timedelta(hours=new_tz)))
    
    return new_time

def process_utc_time(df: pd.DataFrame, str_column: str) -> pd.DataFrame:
    '''
    处理df中带有UTC时区的日期，统一转换为服务器所在时区的时间.

    Parameters
    ----------
    df : DataFrame
        待处理的df.
    str_column : str
        待处理的列.

    Returns
    -------
    DataFrame
        已处理的df.

    '''
    height, width = df.shape
    for i in range(0, height):
        # 解析得到当前日期和当前时区
        dict_parse_result = parse('{date}(UTC{tz:d})', df[str_column][i])
        if dict_parse_result is not None:
            dt_time = datetime.strptime(dict_parse_result['date'], '%Y/%m/%d')
            
            # 第一步：将服务器所在时区的提交时间转换成当前时区的提交时间
            dt_with_tz = convert_to_new_tz(df['C'][i], dict_profile['default_timezone'], dict_parse_result['tz'])
            
            # 第二步：用当前日期替换提交时间中的日期，得到完整的当前时间
            dt_time = datetime.combine(dt_time, dt_with_tz.time())
            
            # 第三步：将当前时区的时间转换成服务器所在时区的时间
            dt_time = convert_to_new_tz(dt_time, dict_parse_result['tz'], dict_profile['default_timezone'])
            
            # 第四步：截取日期，输出为字符串
            df[str_column][i] = dt_time.strftime('%Y/%m/%d')
    
    return df

def load_profile(json_file: str):
    '''
    载入系统配置.

    Parameters
    ----------
    json_file : str
        系统配置JSON文件名.

    Returns
    -------
    None.

    '''
    global dict_profile
    
    with open(json_file, 'r', encoding='utf-8') as profile:
        dict_profile = json.load(profile)   # json数据转成字典

def dump_profile(json_file: str):
    '''
    保存系统配置.

    Parameters
    ----------
    json_file : str
        系统配置JSON文件名.

    Returns
    -------
    None.

    '''
    with open(json_file, 'w', encoding='utf-8') as profile:
        json.dump(dict_profile,         # 待写入数据
                  profile,              # File对象
                  indent=2,             # 空格缩进符，写入多行
                  ensure_ascii=False)   # 显示中文

def connect_db(cur_db: str):
    '''
    用系统配置中给定的参数连接数据库.

    Parameters
    ----------
    cur_db : str
        系统配置中的数据库入口.

    Returns
    -------
    None.

    '''
    db_conn.connect(dict_profile[cur_db]['host'],
                    dict_profile[cur_db]['port'],
                    dict_profile[cur_db]['database'],
                    dict_profile[cur_db]['user'],
                    dict_profile[cur_db]['password'])

def generate_ticket() -> str:
    '''
    Generate one-time ticket.

    Returns
    -------
    str
        The generated ticket.

    '''
    ticket = 'tft-' + str(uuid.uuid4())
    cache_tickets[ticket] = datetime.now()
    return ticket

def check_ticket(ticket: str) -> bool:
    '''
    Check whether the given ticket is valid.

    Parameters
    ----------
    ticket : str
        Ticket to be checked.

    Returns
    -------
    bool
        True - if valid.
        False - if invalid.

    '''
    if ticket[0:4] != 'tft-':
        return False
    
    if ticket in cache_tickets:
        delta = datetime.now() - cache_tickets[ticket]
        del cache_tickets[ticket]
        if delta < timedelta(seconds=30):   # ticket is valid only in 30 seconds
            return True
    
    return False

def add_user(user: User):
    '''
    在Users Cache中添加或替换一个User object.

    Parameters
    ----------
    user : User
        待添加或替换的User object.

    Returns
    -------
    None.

    '''
    users = find_user(user_id=user.property.user_id, cache=True)
    if users:
        cache_users.pop(cache_users.index(users[0]))
    cache_users.append(user)

def authenticate_user(token: str, access_level: str) -> User:
    '''
    认证用户身份和权限.

    Parameters
    ----------
    token : str
        Token.
    access_level : str
        Access level required for taking the action.

    Returns
    -------
    User
        User object.
    None - if not accessible.
    HTTPException - if not authorized.

    '''
#    users = find_user(token=token, cache=True)
    if token[0:4] != 'tfu-':
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Not authorized')
    users = find_user(user_id=int(token[4:]))
    if not users:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Not authorized')
    if users[0].property.deactivated and (users[0].property.account2 == ''):   # account1销户，且account2没有帐号
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Not authorized')
    
    privilege = []
    privilege.extend(bin(users[0].property.privilege)[2:].rjust(8, '0'))
    is_permitted = False
    if (access_level == 'admin') and bool(int(privilege[0])):
        is_permitted = True
    elif (access_level == 'operator') and bool(int(privilege[7])):
        is_permitted = True
    elif (access_level == 'user'):
        is_permitted = True
    if is_permitted:
        return users[0]
    else:
        return None

def delete_user(user_id: int, cache: bool = False) -> bool:
    '''
    在Users Cache以及数据库中删除用户。

    Parameters
    ----------
    user_id : int
        User ID.
    cache : bool, optional
        是否仅在Users Cache中删除，而不在数据库中删除. The default is False.

    Returns
    -------
    bool
        True - if succeeded.
        False - if failed.

    '''
    if not cache:
        result = db_conn.execute("DELETE FROM tf_staff WHERE st_id=%s", params=(user_id), commit=True, rollback=True)
        if (not result) or (db_conn.nrows == 0):
            return False
    
    users = find_user(user_id=user_id, cache=True)
    if users:
        cache_users.pop(cache_users.index(users[0]))
    return True

def find_user(user_id: int = 0, name: str = '', account: str = '', card_id: str = '', mobile: str = '', email: str = '', token: str = '', cache: bool = False) -> List[User]:
    '''
    在DB或者Users Cache中按给定条件查找User object.

    Parameters
    ----------
    user_id : int, optional
        User ID. The default is 0.
    name : str, optional
        用户名及别名. The default is ''.
    account : str, optional
        两个帐号. The default is ''.
    card_id : str, optional
        证件号. The default is ''.
    mobile : str, optional
        手机. The default is ''.
    email : str, optional
        邮箱. The default is ''.
    token : str, optional
        令牌. The default is ''.
    cache : bool, optional
        是否在Users Cache中查找. The default is False.

    Returns
    -------
    List[User]
        List of user object.

    '''
    users = []
    
    if cache:   # Cache
        if user_id != 0:
            for user in cache_users:
                if user.property.user_id == user_id:
                    users.append(user)
        elif name != '':
            for user in cache_users:
                if (user.property.user_name == name) or (user.property.alias == name):
                    users.append(user)
        elif account != '':
            for user in cache_users:
                if (user.property.account1 == account) or (user.property.account2 == account):
                    users.append(user)
        elif card_id != '':
            for user in cache_users:
                if user.property.card_id == card_id:
                    users.append(user)
        elif mobile != '':
            for user in cache_users:
                if user.property.mobile == mobile:
                    users.append(user)
        elif email != '':
            for user in cache_users:
                if user.property.email == email:
                    users.append(user)
        elif token != '':
            for user in cache_users:
                if user.token == token:
                    users.append(user)
    else:   # DB
        userids = []
        
        if user_id != 0:
            userids = [user_id]
        elif name != '':
            userids = User.search_id(name=name)
        elif account != '':
            userids = User.search_id(account=account)
        elif card_id != '':
            userids = User.search_id(card_id=card_id)
        elif mobile != '':
            userids = User.search_id(mobile=mobile)
        elif email != '':
            userids = User.search_id(email=email)
    
        for userid in userids:
            user = new_user()
            if not user:
                return users
            result = user.from_db(user_id=userid)
            if result:
                add_user(user)
                users.append(user)
        
    return users

def new_user() -> User:
    '''
    使用空值生成一个User object.

    Returns
    -------
    User
        User object.

    '''
    user_property = UserProperty(user_id=0,
                                 user_name='',
                                 alias='',
                                 account1='',
                                 account2='',
                                 role='用户',
                                 card_type=dict_profile['staff']['card_type'],
                                 card_id='',
                                 mobile='',
                                 email='',
                                 notes='',
                                 privilege=0,
                                 deactivated=False)
    user = User(user_property)
    return user

def add_item(item: Item):
    '''
    在Items Cache中添加或替换一个Item object.

    Parameters
    ----------
    item : Item
        待添加或替换的Item object.

    Returns
    -------
    None.

    '''
    items = find_item(item_id=item.property.item_id, cache=True)
    if items:
        cache_items.pop(cache_items.index(items[0]))
    cache_items.append(item)

def delete_item(item_id: int, cache: bool = False) -> bool:
    '''
    在Items Cache以及数据库中删除用户。

    Parameters
    ----------
    item_id : int
        Item ID.
    cache : bool, optional
        是否仅在Items Cache中删除，而不在数据库中删除. The default is False.

    Returns
    -------
    bool
        True - if succeeded.
        False - if failed.

    '''
    if not cache:
        result = db_conn.execute("DELETE FROM tf_itemuniqueid WHERE iu_itemid=%s", params=(item_id), rollback=True)
        if not result:
            return False
        
        result = db_conn.execute("DELETE FROM tf_itemadditionalinfo WHERE ia_itemid=%s", params=(item_id), rollback=True)
        if not result:
            return False
        
        result = db_conn.execute("DELETE FROM tf_itemundercontrol WHERE ic_id=%s", params=(item_id), commit=True, rollback=True)
        if (not result) or (db_conn.nrows == 0):
            return False
    
    items = find_item(item_id=item_id, cache=True)
    if items:
        cache_items.pop(cache_items.index(items[0]))
    return True

def find_item(item_id: int = 0, unique_id: str = '', cache: bool = False) -> List[Item]:
    '''
    在DB或者Items Cache中按给定条件查找Item object.

    Parameters
    ----------
    item_id : int, optional
        Item ID. The default is 0.
    unique_id : str, optional
        Unique ID. The default is ''.
    cache : bool, optional
        是否在Items Cache中查找. The default is False.

    Returns
    -------
    List[Item]
        List of item object.

    '''
    items = []
    
    if cache:   # Cache
        if item_id != 0:
            for item in cache_items:
                if item.property.item_id == item_id:
                    items.append(item)
        elif unique_id != '':
            dict_parse_result = parse('id{group:1d}.{uniqueid}', unique_id)
            if not dict_parse_result:
                return items
            for item in cache_items:
                if dict_parse_result['group'] == 1:
                    if item.property.unique_id.group_1 == dict_parse_result['uniqueid']:
                        items.append(item)
                elif dict_parse_result['group'] == 2:
                    if item.property.unique_id.group_2 == dict_parse_result['uniqueid']:
                        items.append(item)
                elif dict_parse_result['group'] == 3:
                    if item.property.unique_id.group_3 == dict_parse_result['uniqueid']:
                        items.append(item)
                elif dict_parse_result['group'] == 4:
                    if item.property.unique_id.group_4 == dict_parse_result['uniqueid']:
                        items.append(item)
    else:   # DB
        itemids = []
        
        if item_id != 0:
            itemids = [item_id]
        elif unique_id != '':
            itemids = Item.search_id(unique_id=unique_id)
        else:
            return items
        
        for itemid in itemids:
            item = new_item()
            if not item:
                return items
            result = item.from_db(item_id=itemid)
            if result:
                add_item(item)
                items.append(item)
    
    return items

def new_item() -> Item:
    '''
    使用空值生成一个Item object.

    Returns
    -------
    Item
        Item object.

    '''
    item_unique_id = ItemUniqueId(group_1='', group_2='', group_3='', group_4='')
    item_additional_info = ItemAdditionalInfo(group_1='', group_2='', group_3='', group_4='', group_5='')
    item_property = ItemProperty(item_id=0,
                                 product_id=1,
                                 product_name='无',
                                 unique_id=item_unique_id,
                                 date='2020-01-01',
                                 operator_id=1,
                                 operator_name='无',
                                 responder_id=1,
                                 responder_name='无',
                                 belong_to=0,
                                 number=0,
                                 location='',
                                 notes='',
                                 status='Good condition',
                                 checked_stuff=False,
                                 checked_label=False,
                                 checked_stock=False,
                                 checked_internal_label=False,
                                 checked_internal_stock=False,
                                 discarded=False,
                                 additional_info=item_additional_info)
    item = Item(item_property)
    return item

def add_product(product: Product):
    '''
    在Products Cache中添加或替换一个Product object.

    Parameters
    ----------
    product : Product
        待添加或替换的Product object.

    Returns
    -------
    None.

    '''
    products = find_product(product_id=product.property.product_id, cache=True)
    if products:
        cache_products.pop(cache_products.index(products[0]))
    cache_products.append(product)

def delete_product(product_id: int, cache: bool = False) -> bool:
    '''
    在Products Cache以及数据库中删除用户。

    Parameters
    ----------
    product_id : int
        Product ID.
    cache : bool, optional
        是否仅在Products Cache中删除，而不在数据库中删除. The default is False.

    Returns
    -------
    bool
        True - if succeeded.
        False - if failed.

    '''
    if not cache:
        result = db_conn.execute("DELETE FROM tf_pconstatistics WHERE pc_productid=%s", params=(product_id), rollback=True)
        if not result:
            return False
        
        result = db_conn.execute("DELETE FROM tf_productinstorage WHERE ps_id=%s", params=(product_id), commit=True, rollback=True)
        if (not result) or (db_conn.nrows == 0):
            return False
    
    products = find_product(product_id=product_id, cache=True)
    if products:
        cache_products.pop(cache_products.index(products[0]))
    return True

def find_product(product_id: int = 0, name: str = '', cache: bool = False) -> List[Product]:
    '''
    在DB或者Products Cache中按给定条件查找Product object.

    Parameters
    ----------
    product_id : int, optional
        Product ID. The default is 0.
    name : str, optional
        Product name. The default is ''.
    cache : bool, optional
        是否在Products Cache中查找. The default is False.

    Returns
    -------
    List[Product]
        List of product object.

    '''
    products = []
    
    if cache:   # Cache
        if product_id != 0:
            for product in cache_products:
                if product.property.product_id == product_id:
                    products.append(product)
        elif name != '':
            for product in cache_products:
                if (product.property.product_name == name) or (product.property.official_name == name) or (product.property.official_model == name):
                    products.append(product)
    else:   # DB
        productids = []
        
        if product_id != 0:
            productids = [product_id]
        elif name != '':
            productids = Product.search_id(name=name)
        else:
            return products
        
        for productid in productids:
            product = new_product()
            if not product:
                return products
            result = product.from_db(product_id=productid)
            if result:
                add_product(product)
                products.append(product)
    
    return products

def new_product() -> Product:
    '''
    使用空值生成一个Product object.

    Returns
    -------
    Product
        Product object.

    '''
    product_statistics = ProductStatistics(total=0, controlled=0, existed=0, on_loan=0, in_stock=0, good_condition=0, bad_condition=0, no_label=0, no_content=0)
    product_property = ProductProperty(product_id=0,
                                       product_name='',
                                       controlled_by=0,
                                       official_name='',
                                       official_model='',
                                       unit='',
                                       currency_unit='CNY',
                                       cost=Decimal('0.00'),
                                       notes='',
                                       statistics=product_statistics)
    product = Product(product_property)
    return product

dict_profile = {}
db_conn = DbConnection()
cache_tickets = {}
cache_users = []
cache_items = []
cache_products = []
